<?php
namespace App\Seip50\Mobile;

use PDO;

class Mobile
{
    public $id = '';
    public $title = "";
    public $laptop = "";
    public $data = '';
    public $user = 'root';
    public $pw = '';
    public $conn = '';

    public function __construct($k = '')
    {
        echo "<pre>";
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=php27', $this->user, $this->pw);
    }

    public function prepare($data = '')
    {

        if (!empty($data['Mobile_model'])) {
            $this->title = $data['Mobile_model'];
        } else {
            $_SESSION['mbl'] = "Required Mobile Model";
        }

        if (!empty($data['laptop_model'])) {
            $this->laptop = $data['laptop_model'];
        } else {
            $_SESSION['lap'] = "Laptop Model Required";
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }


        return $this;
    }

    public function store()
    {
        try {
            $query = "INSERT INTO mobiles(title, laptop, unique_id)
                        VALUES(:title, :l, :u)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':title' => $this->title,
                ':l' => $this->laptop,
                ':u' => uniqid(),
            ));
            header('location:index.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function index()
    {
        try {

            $query = "SELECT * FROM `mobiles` ";
            $q = $this->conn->query($query) or die('Unable to query');
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->data;
        return $this;
    }

    public function show()
    {
        $query = "SELECT * FROM `mobiles` WHERE unique_id=" . "\"$this->id\"";

        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
        return $this;
    }

    public function update()
    {
        $query = "UPDATE `php27`.`mobiles` SET `title` = '$this->title' WHERE `mobiles`.`unique_id` =" . "\"$this->id\"";

        if (mysql_query($query)) {
            $_SESSION['Message'] = "Data successfully updated";
            header('location:index.php');
        }
    }

    public function delete()
    {
        $query = "DELETE FROM `php27`.`mobiles` WHERE `mobiles`.`unique_id` =" . "\"$this->id\"";
        if (mysql_query($query)) {
            $_SESSION['Message'] = "Data deleted";
            header('location:index.php');
        } else {

        }

    }
}












