<?php

include_once '../../../vendor/autoload.php';

use App\SEIP126608\Mobile\mobile;

//print_r($_POST);

$obj = new mobile();

$data = $obj->prepare($_GET)->show();


?>

<form action="update.php" method="post">
                <table>
                    <tr>
                        <td>Company Name:</td>
                        <td>
                            <select name="company">
                                <option><?php echo $data['company_name']; ?></option>
                                <option>Apple</option>
                                <option>Nokia</option>
                                <option>Samsung </option>
                                <option>Symphony</option>                                
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Mobile Model:</td>
                        <td>
                            <select name="model">
                                <option><?php echo $data['mobile_model']; ?></option>
                                <option>ISO-10</option>
                                <option>SYM-10</option>
                                <option>Nok-10 </option>
                                <option>SAM-10</option>                                
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Prize:</td>
                        <td><input type="text" name="prize" value="<?php echo $data['prize']; ?>" ></td>
                    </tr>
                    <tr>
                        <td>Features:</td>
                        <td><textarea cols="22" rows="5" name="features" ><?php echo $data['features']; ?></textarea></td>
                    </tr>
                    <tr>
                        <td>Color</td>
                        <td>
                            <input type="radio" name="color" value="Black"
                                   <?php
                                    if(isset($_GET['id'])){
                                        if($data['color'] == "Black"){
                                            echo 'checked';
                                        }
                                    }
                                   ?>>Black
                            <input type="radio" name="color" value="Brown" 
                                   <?php
                                    if(isset($_GET['id'])){
                                        if($data['color'] == "Brown"){
                                            echo 'checked';
                                        }
                                    }
                                   ?>>Brown
                            <input type="radio" name="color" value="Red"
                                   <?php
                                    if(isset($_GET['id'])){
                                        if($data['color'] == "Red"){
                                            echo 'checked';
                                        }
                                    }
                                   ?>>Red
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="hidden" name="id" value="<?php echo $data['id']; ?>">
                            <input type="submit" name="submit" value="Update">
                        </td>
                    </tr>
                </table>
            </form>