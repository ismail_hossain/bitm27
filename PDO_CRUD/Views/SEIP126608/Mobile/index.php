<?php

include_once '../../../vendor/autoload.php';

use App\SEIP126608\Mobile\mobile;

//print_r($_POST);

$obj = new mobile();

$data = $obj->prepare($_POST)->index();

?>

<h4 style="text-align: center"><a href="create.php">Add New Data</a></h4>

        <?php
            if(isset($_SESSION['message']) && !empty($_SESSION['message'])){
                echo "<p style='text-align:center'>".$_SESSION['message']."</p>";
                unset($_SESSION['message']);   
            }
        ?>

<table  border="1px" style="margin: 0px auto; border-collapse: collapse">
    <tr>
        <th>SL NO</th>
        <th>Company Name</th>
        <th>Mobile Model</th>
        <th>Prize</th>
        <th>Features</th>
        <th>Color</th>
        <th colspan="3">Action</th>
    </tr>
    
    <?php
        $sl = 1;
        foreach ($data as $value) { ?>
    
    <tr>
        <td><?php echo $sl++; ?></td>
        <td><?php echo $value['company_name']; ?></td>
        <td><?php echo $value['mobile_model']; ?></td>
        <td><?php echo $value['prize']; ?></td>
        <td><?php echo $value['features']; ?></td>
        <td><?php echo $value['color']; ?></td>
        <td><a href="show.php?id=<?php echo $value['id']; ?>">Details</a></td>
        <td><a href="edit.php?id=<?php echo $value['id']; ?>">Edit</a></td>
        <td><a href="delete.php?id=<?php echo $value['id']; ?>">Delete</a></td>
        
    </tr>
            
    <?php } ?>
    
</table>


