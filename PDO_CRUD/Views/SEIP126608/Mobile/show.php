<?php

include_once '../../../vendor/autoload.php';

use App\SEIP126608\Mobile\mobile;

//print_r($_POST);

$obj = new mobile();

$data = $obj->prepare($_GET)->show();


?>

<table  border="1px" style="margin: 0px auto; border-collapse: collapse">
    <tr>
        <th>SL NO</th>
        <th>Company Name</th>
        <th>Mobile Model</th>
        <th>Prize</th>
        <th>Features</th>
        <th>Color</th>
        <th colspan="2">Action</th>
    </tr>
    
    <tr>
        <td><?php echo $data['id'] ?></td>
        <td><?php echo $data['company_name']; ?></td>
        <td><?php echo $data['mobile_model']; ?></td>
        <td><?php echo $data['prize']; ?></td>
        <td><?php echo $data['features']; ?></td>
        <td><?php echo $data['color']; ?></td>
        <td><a href="index.php">Back</a></td>
        <td><a href="create.php">Add New Data</a></td>
        
    </tr>
    
</table>
