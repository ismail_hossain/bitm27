<?php
session_start();
?>


<html>
    <head>
        <title>CRUD Excercise</title>
    </head>
    <body>
        
        <fieldset style="width: 300px; margin: 0px auto">
            <legend><b><i>Insert Mobile Info</i></b></legend>
            <form action="store.php" method="post">
                <table>
                    <tr>
                        <td>Company Name:</td>
                        <td>
                            <select name="company">
                                <option>Choose your Company</option>
                                <option>Apple</option>
                                <option>Nokia</option>
                                <option>Samsung </option>
                                <option>Symphony</option>                                
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Mobile Model:</td>
                        <td>
                            <select name="model">
                                <option>Choose Mobile Model</option>
                                <option>ISO-10</option>
                                <option>SYM-10</option>
                                <option>Nok-10 </option>
                                <option>SAM-10</option>                                
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Prize:</td>
                        <td><input type="text" name="prize" ></td>
                    </tr>
                    <tr>
                        <td>Features:</td>
                        <td><textarea cols="22" rows="5" name="features" ></textarea></td>
                    </tr>
                    <tr>
                        <td>Color</td>
                        <td>
                            <input type="radio" name="color" value="Black" >Black
                            <input type="radio" name="color" value="Brown" >Brown
                            <input type="radio" name="color" value="Red">Red
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="reset" name="submit" value="Reset">
                            <input type="submit" name="submit" value="Submit">
                        </td>
                    </tr>
                </table>
            </form>
        </fieldset>
    </body>
</html>