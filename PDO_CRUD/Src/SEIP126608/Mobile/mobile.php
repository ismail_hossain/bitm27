<?php
namespace App\SEIP126608\Mobile;
use PDO;

class mobile {
    public $id = '';
    public $company = '';
    public $model = '';
    public $prize = '';
    public $features = '';
    public $color = '';
    public $data = '';
    public $conn = '';


    public $user = 'root';
    public $pass = '';



    public function __construct(){
        session_start();
        $this->conn = new PDO("mysql:host=localhost;dbname=crud_cxcer", $this->user, $this->pass);
        
    }


    public function prepare($data = ''){
 
        
        if(array_key_exists("id", $data)){
            $this->id = $data['id'];
        }
        if(array_key_exists("company", $data)){
            $this->company = $data['company'];
        }
        if(array_key_exists("model", $data)){
            $this->model = $data['model'];
        }
        if(array_key_exists("prize", $data)){
            $this->prize = $data['prize'];
        }
        if(array_key_exists("features", $data)){
            $this->features = $data['features'];
        }
        if(array_key_exists("color", $data)){
            $this->color = $data['color'];
        }
        return $this;
    }    
        
        public function store(){
            
            try{
            $query ="INSERT INTO mobile( company_name, mobile_model, prize, features, color )
                    VALUES ( :company_name, :mobile_model, :prize, :features, :color)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':company_name' => $this->company,
                ':mobile_model' => $this->model,
                ':prize' => $this->prize,
                ':features' => $this->features,
                ':color' => $this->color,
            ));
            $_SESSION['message'] = "Successfully Inserted";
            header('location:index.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        
     }
     public function index(){
         try {
             $query = "SELECT * FROM mobile";
             $result = $this->conn->query($query) or die("Unable to query");
             while ($row = $result->fetch(PDO::FETCH_ASSOC)){
                 $this->data[] = $row;
             }
             
         } catch (PDOException $e) {
             echo 'Error:'. $e->getMessage();
             
         }
         return $this->data;
         return $this;
     }
     public function show(){
         
         try {
             $query = "SELECT * FROM mobile WHERE id = ".$this->id;
             $stmt = $this->conn->prepare($query);
             $stmt->execute();
             $row = $stmt->fetch(PDO::FETCH_ASSOC);
                 $this->data = $row;
             
         } catch (PDOException $e) {
             echo 'Error:'. $e->getMessage();
             
         }
         return $this->data;
         return $this;
     }
     public function update(){
         try{
            $query ="UPDATE mobile SET company_name = :company_name,
                                       mobile_model = :mobile_model,
                                       prize = :prize,
                                       features = :features,
                                       color = :color
                    WHERE id = ".  $this->id;                   
                    
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':company_name' => $this->company,
                ':mobile_model' => $this->model,
                ':prize' => $this->prize,
                ':features' => $this->features,
                ':color' => $this->color,
            ));
            $_SESSION['message'] = "Successfully Updated";
            header('location:index.php');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
     }
     public function delete(){
         try{
             echo $this->id;
             
         $query = "DELETE  FROM mobile WHERE id = '$this->id'";
         $this->conn->exec($query);
         $_SESSION['message'] = "Successfully Deleted";
         header('location: index.php');
         
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
     
    }
     
}
