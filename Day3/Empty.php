<?php

/* /* Returns FALSE if var exists and has a non-empty, non-zero value. Otherwise returns TRUE.

The following things are considered to be empty:

    "" (an empty string)
    0 (0 as an integer)
    0.0 (0 as a float)
    "0" (0 as a string)
    NULL
    FALSE
    array() (an empty array)
    $var; (a variable declared, but without a value)*/
$a = 10;
$b= "";
$c= null;
echo "$a";

if(empty($b)){
    echo "this is empty";
}  else {
    
    echo "not empty";
    
}
 ?>