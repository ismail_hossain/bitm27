<?php

 namespace App\SEIP121978\Mobile;

 use PDO;

 class Mobile {

     public $id = "";
     public $title = "";
     public $data = "";
     public $conn = '';
     public $user = 'root';
     public $pass = '';

     public function __construct() {
         session_start();
         $this->conn = new PDO("mysql:host=localhost;dbname=home", $this->user, $this->pass);
     }

     public function papare($data = '') {
//        print_r($data);
//        die();
         if (array_key_exists('name', $data)) {
             $this->title = $data["name"];
         }
         if (array_key_exists('mobile', $data)) {
             $this->data = $data["mobile"];
         }
         if (array_key_exists('id', $data)) {
             $this->id = $data["id"];
         }

//        print_r($this->title);
         return $this;
     }

     public function store() {
         try {
             $query = "INSERT INTO soft_delete( name, mobile, is_delete)
                    VALUES ( :name, :mobile,:is_delete)";
             $stmt = $this->conn->prepare($query);
             $stmt->execute(array(
               ':name' => $this->title,
               ':mobile' => $this->data,
               ':is_delete' => 0,
             ));
             $_SESSION['message'] = "Successfully Inserted";
             header('location:index.php');
         } catch (PDOException $e) {
             echo 'Error: ' . $e->getMessage();
         }
     }

     public function index() {
         try {
             $query = "SELECT * FROM soft_delete where is_delete =0 ";
             $result = $this->conn->query($query) or die("Unable to query");
             while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                 $this->data[] = $row;
             }
         } catch (PDOException $e) {
             echo 'Error:' . $e->getMessage();
         }
         return $this->data;
         return $this;
     }

     public function show() {
         try {
             $query = "SELECT * FROM soft_delete WHERE id = " . $this->id;
             $stmt = $this->conn->prepare($query);
             $stmt->execute();
             $row = $stmt->fetch(PDO::FETCH_ASSOC);
             $this->data = $row;
         } catch (PDOException $e) {
             echo 'Error:' . $e->getMessage();
         }
         return $this->data;
         return $this;
     }

     public function delete() {

         $quory = "DELETE FROM `mobiles` WHERE `mobiles`.`id` = "
                    . "$this->id";

         if (mysql_query($quory)) {
             header("location:index.php");
             $_SESSION['message'] = "succefully Deleted";
         }

         return $this;
     }

     public function edit() {
         try {

//            echo $this->id;
//            die();
//             
             $query = "UPDATE soft_delete SET name = :n, mobile = :m, is_delete= :is WHERE id = :i";
             $stmt = $this->conn->prepare($query);
//             print_r($stmt);
             $stmt->execute(array(
               ':i' => $this->id,
               ':n' => $this->title,
               ':m' => $this->data,
               ':is' => 0,
             ));

//             if ($stmt->execute(array(
//                          ':n' => $this->title,
//                          ':m' => $this->data,
//                          ':i' => $this->id,
//                        ))) {
//                 echo "something going wrong";
//             }
             $_SESSION['message'] = "Successfully Updated";
             header('location:index.php');
         } catch (PDOException $e) {
             echo 'Error: ' . $e->getMessage();
         }
//        $qurry= "UPDATE `mobiles` SET `title` = "
//                . "'$this->title'"
//                . "WHERE `mobiles`.`id` ="
//                . "$this->id";
//          
//            if(mysql_query($qurry)){
//            header("location:index.php");
//            $_SESSION['message']= "succefully Updated";    
//            }
//            
//        return $this;
     }

 }
 